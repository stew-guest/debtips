# Gitlab

## Branch scheme

Some groups (at least the PAPT and DMPT teams) use `gbp` to build packages. To support this, we need to follow a specific branching scheme.  Repositories will have (at a minimum) these branches/tags:

- master: contains `upstream` + `debian/*`
- upstream: only contains original content from upstream releases.  One commit per release -- don't preserve upstream's commit history
- pristine-tar: contains binaries used to recreate a binary identical tarball from the upstream branch
- upstream/1.9.0: tag from the upstream branch of an upstream release
- debian/1.9.0-1: tag from the master branch of a packaged release. Only create this after an upload was accepted. Generally, let your sponsor tag this.

Debian uses `quilt` to patch the original sources. Those patches are managed in `debian/patches`.  It *is* possible to auto-generate `debian/patches` and `debian/changelog` from a branch, but I don't know how to do it yet.

## SSH

You may need to login with an SSH key to get read or write acces to some repositories.

1. Generate an ssh key and copy it to your clipboard

```
ssh-keygen -t ed25519 -C "stew@ferg.aero"
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

2. In gitlab, click on your picture (top-right), then **Settings**.
3. On the left-hand menu, click **SSH Keys**
4. Paste your clipboard into the "Key" box and give it a title identifying your machine
5. Click **Add Key**

## Continuous Integration:

To setup CI in the gitlab repository use the browser and do this:

- Navigate to "Settings" > "CI/CD" > "General pipelines"
- set "Custom CI config path" to `debian/.gitlab-ci.yml`

Add `debian/.gitlab-ci.yml` to your project:

```
image: registry.salsa.debian.org/salsa-ci-team/ci-image-git-buildpackage:latest

pages:
  stage: deploy
  artifacts:
    paths:
      - public
  script:
    - gitlab-ci-git-buildpackage
    - gitlab-ci-lintian
    - gitlab-ci-aptly
```
