# Git cheatsheet

## Cloning

Use `git clone` to clone a repository. SSH 

An SSH command like the one below will often require login credentials to clone:

```
git clone git@salsa.debian.org:stew-guest/debtips.git
```

An HTTPS command like the one below will often let you clone without login credentials:

```
git clone https://salsa.debian.org/stew-guest/debtips.git
```

## Pull changes

You'll need to track a remote branch to pull from it.  *I think you track master by default*. 

To list remote branches use: 

```
git branch -r
```

Then to track a branch, use: 

```
git branch --track master origin/master
git fetch --all
git pull --all
```

## Staging

Changes must be staged before they can be commited:

```
git add debian/changelog   # stages current changes to debian/changelog
                           # if the file no longer exists, the deletion will be staged
                           # if the file is new, the addition will be staged
git add --all    # Stages EVERYTHING (deletions, additions, changes)
```

To unstage (undo stage) a change:

```
git reset filename
```

To unstage a deleted file:

```
git reset -- filename
```

## Commiting

```
git status                   # Review what is/isn't staged first
git diff --staged            # Review content that you're about to commit
git commit -m "Commit message"
```

To add newly staged content to the most recent commit, or to fix the commit message of it:

```
git commit --amend
```

You may also change the commit message of an older commit:

```
git checkout 7c2ce6627135b71fed8b519c1315374d66bc3e03
git commit --amend 
```

If you need to add content to an older commit:

```
git rebase --interactive 'b1e042fdb47aef3e2304dce8d0d3a3cb97bdd6bb^'
``` 

Then then change `pick` to `edit` for the commit you want to change, save, make the changes, stage them, then:

```
git commit --amend '-S'
git rebase --continue
```

## Diff

To review unstaged changes:

```
git diff
```

To review staged changes (about to be commited):

```
git diff --staged
```

To review changes from a specific commit:

```
git diff b1e042fdb47aef3e2304dce8d0d3a4cb97bdd6bb~ b1e042fdb47aef3e2304dce8d0d3a4cb97bdd6bb
```

## Remote operations

```
git remote get-url origin
git remote set-url origin
```

## Cleaning up

```
git clean -df .    # Remove all unversioned files
git checkout *     # Revert all locally changed files
```

## Branches

Create a local <feature_branch>

```
git branch <feature_branch>
```

Checkout the local <feature_branch>

```
git branch <feature_branch>
```

Pull, checkout, and track a remote <feature_branch>

```
git checkout --track origin/<feature_branch>
```

Push the <feature_branch> to the repository

```
git push origin <feature_branch>
```

Push all branches to the repository (doesn't

```
git push --all origin
```

To delete a local branch: 

```
git branch -d <feature_branch>
```

To delete a remote branch:

```
git push origin --delete <feature_branch>
```

## Tags

See https://git-scm.com/book/en/v2/Git-Basics-Tagging for details.

### List all tags:

```
git tag
```

> Note that this list *does* get updated with `git fetch --all`

### Create a tag from the current state:

```
git tag upstream/1.9.0
git tag debian/1.9.0-1
```

### Switch to a tag:

```
git checkout upstream/1.9.0
```

> Commits on tags are unreachable.  If you want to make a commit, make a branch first.

### Push tags:

```
git push upstream/1.9.0
 or
git push --tags 
```

> `git push` doesn't push tags. You'll need to do those explicitly using one of the commands above.

### Delete tags:

```
git tag -d debian/1.9.0-1                   # Deletes locally
git push origin :refs/tags/debian/1.9.0-1   # Push deletion to origin
```



## Patches

### Export a commit to a patch file:

```
git format-patch -1 <SHA1>
```

### Export the last three commits to patch files:

```
git format-patch -3 HEAD
```

### Import patch files:

```
git am < ../0001-Minimal-changes-to-debian-to-get-1.9.0-to-build-with.patch
git am < ../0002-Adding-python3-build-rules-and-fixing-lintian-messag.patch
git am < ../0003-Adding-CI-script.patch
```

## GPG signing with git

You can sign your git commits with GPG.  This can be used to verify that the commits really came from you.

```
git --list-secret-keys --keyid-format LONG stew@ferg.aero
git config --global user.signingkey 186CF99F98E75ABD
git config --global commit.gpgsign true
```
