https://github.com/corydolphin/flask-cors/archive/3.0.7.tar.gz -O python-flask-cors_3.0.7.orig.tar.gz

mkdir python-flask-cors && cd python-flask-cors
git init
git checkout -b upstream
tar -xzvf ../python-flask-cors_3.0.7.orig.tar.gz --strip-components=1

git add * .gitignore .travis.yml
git commit -m "Upstream release 3.0.7"

git checkout -b master

pristine-tar commit ../python-flask-cors_3.0.7.orig.tar.gz

# Generate debian/*
dh-make

# Edit everything you need




Be sure to add Build-dep:
python-flask
python3-flask
python-nose
python3-nose
