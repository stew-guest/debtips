# Mentors

Mentors is an apt repository similar to the main official debian repository, except that it is meant to be used for sponsored maintainers to send source packages to their sponsors.

Create `~/.dput.cf` or append the following content to it:

```
[mentors]
fqdn = mentors.debian.net
incoming = /upload
method = https
allow_unsigned_uploads = 0
progress_indicator = 2
# Allow uploads for UNRELEASED packages
allowed_distributions = .*

[mentors-ftp]
fqdn = mentors.debian.net
login = anonymous
progress_indicator = 2
passive_ftp = 1
incoming = /pub/UploadQueue/
method = ftp
allow_unsigned_uploads = 0
# Allow uploads for UNRELEASED packages
allowed_distributions = .*
```

Now, whenever you build a (signed) package, you may upload it to mentors with: 

```
dput mentors ../foo_1.9.0-1_amd64.changes
```

It may take a few minutes, but you'll find it on the mentors site at https://mentors.debian.net/package/foo.

On that page, once you log in you can add comments or set the **Needs a sponsor** flag to try and get the attention of sponsors.
