# Tuna

## Python3 vs Python2. 

Tuna CLI can be run with python2 or python3.  GUI mode has a dependency on python-glade2 which is not available for python3.  

Version 0.13+ cannot be compiled with python2.  

Therefore, we are packaging version 0.12 with python2.  If the upstream source is changed to replace its python-glade2 dependency with something else, then we can update this package and compile with python3. 

## Python 2 build instructions:
```
wget https://git.kernel.org/pub/scm/utils/tuna/tuna.git/snapshot/tuna-0.12.tar.gz
```
Then:
```
set -e

cd ~/src 
rm -rf tuna-0.12
rm -f tuna_*

tar -xzf tuna-0.12.tar.gz
cd tuna-0.12

DEBNAME="Stewart Ferguson"
dh_make --email stew@ferg.aero --copyright=gpl -s --file=../tuna-0.12.tar.gz --yes

cp ~/src/wc/packages/tuna/debian/rules debian/rules
cp ~/src/wc/packages/tuna/debian/control debian/control

fakeroot dpkg-buildpackage -us -uc
```

Then copy files from svn: `^/packages/tuna/debian` to the `debian` directory. 

Then:
```
fakeroot dpkg-buildpackage -us -uc
```


## Python 3 build instructions: 
```
wget http://git.kernel.org/pub/scm/utils/tuna/tuna.git/snapshot/tuna-0.14.tar.gz
```
Then: 
```
tar -xzf tuna-0.14.tar.gz
cd tuna-0.14
DEBNAME="Stewart Ferguson"
dh_make --email stew@sim-international.com --copyright=gpl -s --file=../tuna-0.14.tar.gz --yes
cp ~/src/wc/packages/tuna/debian/rules debian/rules
cp ~/src/wc/packages/tuna/debian/control debian/control
```

You will need to edit rules and control at this point

- In `control`, Replace `python` dependencies with `python3` dependencies
- In `rules`, Replace `python` with `python3`.

Finally, build with:
```
fakeroot dpkg-buildpackage -us -uc
```
