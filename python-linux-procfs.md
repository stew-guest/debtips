## Get the original source
```
wget https://git.kernel.org/pub/scm/libs/python/python-linux-procfs/python-linux-procfs.git/snapshot/python-linux-procfs-0.6.tar.gz
```

## Build it (this can be run as a script easily)
```
# Exit if we get errors
set -e

# Clean up from previous run if this isn't the first time we are building
cd ~/src 
rm -rf python-linux-procfs-0.6
rm -f python-linux-procfs_*
rm -f python3-linux-procfs_*

# Unpack the archive
tar -xzf python-linux-procfs-0.6.tar.gz
cd python-linux-procfs-0.6

# Generate the build instruction templates
DEBNAME="Stewart Ferguson"
dh_make --email stew@ferg.aero --copyright=gpl -i --file=../python-linux-procfs-0.6.tar.gz -p python-linux-procfs_0.6 --yes

# Fill in the details
cp ~/src/wc/packages/python3-linux-procfs/debian/control debian/control
cp ~/src/wc/packages/python3-linux-procfs/debian/rules debian/rules

# Build and package
fakeroot dpkg-buildpackage -us -uc
```
The packages are now all in `~/src`

