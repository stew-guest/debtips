# sbuild

sbuild is what buildd uses to build packages.  It install dependencies and builds everything in a chroot.

## Install/Configure

```
sudo apt install sbuild
sudo sbuild-adduser $LOGNAME
# Use `newgrp sbuild` to add yourself to the new group in the current shell
sudo sbuild-createchroot --include=eatmydata,ccache,gnupg unstable /var/chroot/unstable-amd64-sbuild http://deb.debian.org/debian
```

Add this to `~/.sbuildrc`: 

```
$distribution = 'unstable';
```

## Running

First make sure the chroot is up to date: 

```
sudo sbuild-update -udcar unstable-amd64-sbuild
```

To build, be in your source directory and run:

```
sbuild
```
